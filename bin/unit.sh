#!/bin/bash

test-library() {
    library=$1
    echo -n "Testing $library ... "
    if diff -q <(env LD_PRELOAD=../lib/$library ../bin/unit_test) <(test-output); then
    	echo "success"
    else
    	echo "failure"
	diff -u <(env LD_PRELOAD=../lib/$library ../bin/unit_test) <(test-output)
    fi
}

test-output() {
    cat <<EOF
mallocs:   2
frees:     1
reuses:    1
grows:     1
splits:    0
coalesces: 0
blocks:    1
requested: 20
max heap:  16
EOF
}

test-library libmalloc-ff.so
test-library libmalloc-nf.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so
