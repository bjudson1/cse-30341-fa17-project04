//Brenden Judson
//DJ Chao
/* malloc.c: simple memory allocator -----------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

/* Macros --------------------------------------------------------------------*/

#define ALIGN4(s)           (((((s) - 1) >> 2) << 2) + 4)
#define BLOCK_DATA(b)       ((b) + 1)
#define BLOCK_HEADER(ptr)   ((struct block *)(ptr) - 1)

/* Block structure -----------------------------------------------------------*/

struct block {
    size_t        size;
    struct block *next;
    bool          free;
};

/* Global variables ----------------------------------------------------------*/
bool registered = false;
struct block *FreeList = NULL;
struct block *last_selected = NULL;


/* counters for mallocs, frees, reuses, grows */
int MALLOCS    = 0;
int FREES      = 0;
int REUSES     = 0;
int GROWS      = 0;
int SPLITS     = 0;
int COALESCES  = 0;
int FREEBLOCKS = 0;
size_t REQUESTED = 0;
size_t MAXHEAP = 0;

void onExit(){
    //count the free blocks
    struct block *temp = FreeList;
    while(temp){
        FREEBLOCKS++;
        temp = temp->next;
    }

    //print results
    printf("mallocs:   %d\n", MALLOCS);
    printf("frees:     %d\n", FREES);
    printf("reuses:    %d\n", REUSES);
    printf("grows:     %d\n", GROWS);
    printf("splits:    %d\n", SPLITS);
    printf("coalesces: %d\n", COALESCES);
    printf("blocks:    %d\n", FREEBLOCKS);
    printf("requested: %zu\n", REQUESTED);
    printf("max heap:  %zu\n", MAXHEAP);
}


/* Find free block -----------------------------------------------------------*/

struct block *find_free(struct block **last, size_t size) {
    struct block *curr = FreeList;

    /* First fit */
    #if defined FIT && FIT == 0
        //loop until find free block who is big enough
        while(curr && !(curr->free && curr->size >= size)){
            *last = curr;
            curr  = curr->next;
        }
    #endif

    /* Next fit */
    #if defined FIT && FIT == 1
        //if not first time start at last selected block
        if(last_selected != NULL)
            curr = last_selected;

        //do first fit
        //loop until find free block who is big enough
        while(curr && !(curr->free && curr->size >= size)){
            *last = curr;
            curr  = curr->next;
        }

        //go to begining of list if youve reached the end
        if(!curr){
            curr = FreeList;
            //do first fit
            //loop until find free block who is big enough or reach last selected
            //i dont get why not && curr != last_selected
            while(curr && !(curr->free && curr->size >= size)){
                *last = curr;
                curr  = curr->next;
            }

            if(curr == last_selected){
                curr = NULL;
            }
        }

        //set last selected block
        if(curr != NULL)
            last_selected = curr;
    #endif

    /* Best fit */
    #if defined FIT && FIT == 2
        struct block *smallest = NULL;

        //find smallest free block
        while(curr){
            //if free block + large enough
            if(curr->free && curr->size >= size){
                //smallest hasnt been set
                if(smallest == NULL)
                    smallest = curr;
                //smaller than smallest
                else if(curr->size < smallest->size)
                    smallest = curr;
            }

            *last = curr;
            curr = curr->next;
        }

        //select smallest
        curr = smallest;
    #endif

    /* Worst Fit */
    #if defined FIT && FIT == 3
        struct block *largest = NULL;
        //find largest free block
        while(curr){
            //if free block + large enough
            if(curr->free && curr->size >= size){
                //largest hasnt been set
                if(largest == NULL)
                    largest = curr;
                //larger than largest
                else if(curr->size > largest->size)
                    largest = curr;
            }

            *last = curr;
            curr = curr->next;
        }

        //select largest
        curr = largest;
    #endif

    return curr;
}

/* Grow heap -----------------------------------------------------------------*/

struct block *grow_heap(struct block *last, size_t size){
    /* Request more space from OS */
    struct block *curr = (struct block *)sbrk(0);
    struct block *prev = (struct block *)sbrk(sizeof(struct block) + size);

    assert(curr == prev);

    /* OS allocation failed */
    if (curr == (struct block *)-1) {
        return NULL;
    }

    /* Update FreeList if not set */
    if (FreeList == NULL) {
        FreeList = curr;
    }

    /* Attach new block to prev block */
    if (last) {
        last->next = curr;
    }

    /* Update block metadata */
    curr->size = size;
    curr->next = NULL;
    curr->free = false;

    //update counters
    GROWS++;
    MAXHEAP += size;

    return curr;
}

/* Allocate space ------------------------------------------------------------*/

void yo(){
    
} 

void *malloc(size_t size){
    //register "atexit()" function
    if(!registered){
        int i = atexit(onExit);
            if(i != 0) {
                fprintf(stderr, "cannot set exit function\n");
                exit(EXIT_FAILURE);
            }
        registered = true;
    }

    /* Align to multiple of 4 */
    size = ALIGN4(size);

    //add to amount requested
    REQUESTED += size;

    //size required to create block
    size_t new_size = size + sizeof(struct block);

    /* Handle 0 size */
    if (size == 0) {
        return NULL;
    }

    /* Look for free block */
    struct block *last = last_selected;
    struct block *next = find_free(&last, size);

    /* Split free block? */
    //if there is free block and it is too large
    if(next && (next->size > new_size)){
        //--Empty Block--//
        //create new block
        struct block *new = (void*)BLOCK_DATA(next) + size;
        //new block size is extra space
        new->size = next->size - new_size;
        //new block is free
        new->free = true;
        //it points to what was next                                  
        new->next = next->next;

        //--Filled Block--//
        //it is the allocated size
        next->size = size;
        //its full
        next->free = false;
        //it points to the new block
        next->next = new; 
        
        //update counter
        SPLITS++;
    }

    /* Could not find free block, so grow heap */
    if(next == NULL){
        next = grow_heap(last, size);
    }
    
    //reused a free block
    else{
        REUSES++;
    }

    /* Could not find free block or grow heap, so just return NULL */
    if(next == NULL){
        return NULL;
    }

    /* Mark block as in use */
    next->free = false;

    //count malloc
    MALLOCS++;

    /* Return data address associated with block */
    return BLOCK_DATA(next);
}

/* Reclaim space -------------------------------------------------------------*/

void free(void *ptr){
    if(ptr == NULL){
        return;
    }

    /* Mark block as free */
    struct block *curr = BLOCK_HEADER(ptr);
    assert(curr->free == 0);
    curr->free = true;

    /* Coalesce free blocks? */
    //temp var to loop through free blocks
    struct block *temp = curr;

    //while there is a block + a next block
    while(temp && temp->next){
        //if they both are free, coalesce
        if(temp->free && temp->next->free){
            //add next blocks size to current block
            temp->size += sizeof(struct block) + temp->next->size;
            //skip absorbed block
            temp->next = temp->next->next;
            //update couter
            COALESCES++;
        }
        temp = temp->next;
    }

    //count free
    FREES++;

}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: --------------------------------*/
