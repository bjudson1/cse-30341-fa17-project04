dCC=       	gcc
CFLAGS= 	-g -gdwarf-2 -std=gnu99 -Wall
LDFLAGS=
LIBRARIES=      lib/libmalloc-ff.so \
		lib/libmalloc-nf.so \
		lib/libmalloc-bf.so \
		lib/libmalloc-wf.so

TESTS=	bin/unit_test \
		bin/test_00 \
		bin/test_01 \
		bin/test_02 \
		bin/test_03


all:    $(LIBRARIES) $(TESTS)


lib/libmalloc-ff.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=0 -o $@ $< $(LDFLAGS)

lib/libmalloc-nf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=1 -o $@ $< $(LDFLAGS)

lib/libmalloc-bf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=2 -o $@ $< $(LDFLAGS)

lib/libmalloc-wf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=3 -o $@ $< $(LDFLAGS)


bin/unit_test:		tests/unit_test.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_00:		tests/test_00.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_01:		tests/test_01.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_02:		tests/test_02.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_03:		tests/test_03.o
	$(CC) $(CFLAGS) -o $@ $<


tests/unit_test.o:		tests/unit_test.c
	$(CC)  $(CFLAGS) -c -o $@ $<

tests/test_00.o:		tests/test_00.c
	$(CC)  $(CFLAGS) -c -o $@ $<

tests/test_01.o:		tests/test_01.c
	$(CC)  $(CFLAGS) -c -o $@ $<

tests/test_02.o:		tests/test_02.c
	$(CC)  $(CFLAGS) -c -o $@ $<

tests/test_03.o:		tests/test_03.c
	$(CC)  $(CFLAGS) -c -o $@ $<


clean:
	rm -f $(LIBRARIES) *.o $(TESTS)

.PHONY: all clean


