CSE.30341.FA17: Project 04
==========================

This is the documentation for [Project 04] of [CSE.30341.FA17].

Members
-------

1. Brenden Judson (bjudson1@nd.edu)
2. DJ Chao (dchao@nd.edu)

Design
------

> 1. You will need to implement splitting of free blocks:
>
>   - When should you split a block?
		When it is larger than the required size.
>
>   - How should you split a block?
		Spit it into 2 pieces. The first being the size you 
		allocated and the second being the remainder of the block. 
		The second piece will remain free.

> 2. You will need to implement coalescing of free blocks:
>
>   - When should you coalescing block?
>		When the current block and next block are both free.

>   - How should you coalesce a block?
		Point the current block past the next block and update its size.

> 3. You will need to implement Next Fit.
>
>   - What information do you need to perform a Next Fit?
>		The last selected block

>   - How would you implement Next Fit?
		Start at the last selected block. From there,
		perform First Fit until you find a match.		

> 4. You will need to implement Best Fit.
>
>   - What information do you need to perform a Best Fit?
		You need to keep track of the smallest free block
		that is larger than the allocated size.
>
>   - How would you implement Best Fit?
		I would travesrs the free list and find the smallest
		free block that is larger than the allocated size.

> 5. You will need to implement Worst Fit.
>
>   - What information do you need to perform a Worst Fit?
>		You need to keep track of the largest free block
		that is larger than the allocated size.

>   - How would you implement Worst Fit?
		I would travesrs the free list and find the largest
		free block that is larger than the allocated size.

> 6. You will need to implement tracking of different information.
>
>   - What information will you want to track?
		mallocs
		frees
		reuses
		grows
		splits
		coalesces
		blocks
		requested amount
		max heap
>
>   - How will you update these trackers?
		Updating there counters in their correspinding
		functions.
>
>   - How will you report these trackers when the program ends?
		Register atexit function.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 04]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project04.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
